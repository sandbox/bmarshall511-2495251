INTRODUCTION
------------
Adds the ability to implement the Tynt service to retrieve copy & paste
analytics and append additional copy to text that has been copied.

For more information on Tynt, please check out their website: http://tynt.com/

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Configure user permissions in Administration » People » Permissions:


   - Administer Tynt Service Settings (Statistics module)


     The top-level administration categories require this permission to be
     accessible. The administration menu will be empty unless this permission
     is granted.

MAINTAINERS
-----------
Current maintainers:
 * Ben Marshall (bmarshall) - https://www.drupal.org/u/bmarshall
