<?php
/**
 * @file
 * Provide enhanced text selections & tracking via the Tynt copy/paste service.
 */

/**
 * Implements hook_menu().
 */
function tynt_service_menu() {

  // Tynt admin configuration page.
  $items['admin/config/user-interface/tynt-service'] = array(
    'title'            => 'Tynt Service',
    'description'      => 'Update Tynt configuration settings.',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('tynt_service_admin_settings'),
    'access arguments' => array('administer tynt service settings'),
    'file'             => 'tynt_service.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function tynt_service_permission() {

  // Create the administer tynt settings permission.
  return array(
    'administer tynt service settings' => array(
      'title'       => t('Administer Tynt Service Settings'),
      'description' => t('Allows access to update the Tynt configuration settings.'),
    ),
  );
}

/**
 * Implements theme_preprocess_page().
 */
function tynt_service_preprocess_page() {
  global $user;

  $tynt_service_site_guid = variable_get('tynt_service_site_guid', '');

  if (!empty($tynt_service_site_guid) && _tynt_service_visibility_pages() && _tynt_service_visibility_roles($user) && _tynt_service_visibility_custom()) {
    $script = "if(document.location.protocol=='http:'){
 var Tynt=Tynt||[];Tynt.push('" . $tynt_service_site_guid . "');
 (function(){var s=document.createElement('script');s.async=\"async\";s.type=\"text/javascript\";s.src='http://tcr.tynt.com/ti.js';var h=document.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);})();
}";

    drupal_add_js($script, array('type' => 'inline'));
  }
}

/**
 * Tynt visibility role check.
 *
 * Based on visibility setting this function returns TRUE if Tynt code should
 * be added for the current role and otherwise FALSE.
 */
function _tynt_service_visibility_roles($account) {

  $enabled = FALSE;
  $roles   = variable_get('tynt_service_roles', array());

  if (array_sum($roles) > 0) {

    // One or more roles are selected for tracking.
    foreach (array_keys($account->roles) as $rid) {

      // Is the current user a member of one role enabled for tracking?
      if (isset($roles[$rid]) && $rid == $roles[$rid]) {

        // Current user is a member of a role that should be tracked.
        $enabled = TRUE;
        break;
      }
    }
  }
  else {

    // No role is selected for tracking, therefor all roles should be tracked.
    $enabled = TRUE;
  }

  return $enabled;
}

/**
 * Tynt visibility page check.
 *
 * Based on visibility setting this function returns TRUE if Tynt code should
 * be added to the current page and otherwise FALSE.
 */
function _tynt_service_visibility_pages() {
  static $page_match;

  // Cache visibility setting in hook_init for hook_footer.
  if (!isset($page_match)) {

    $visibility = variable_get('tynt_service_visibility', 0);
    $pages = variable_get('tynt_service_pages', '');

    // Match path if necessary.
    if (!empty($pages)) {
      if ($visibility < 2) {
        $path = drupal_get_path_alias($_GET['q']);
        // Compare with the internal and path alias (if any).
        $page_match = drupal_match_path($path, $pages);
        if ($path != $_GET['q']) {
          $page_match = $page_match || drupal_match_path($_GET['q'], $pages);
        }
        // When $visibility has a value of 0, the block is displayed on
        // all pages except those listed in $pages. When set to 1, it
        // is displayed only on those pages listed in $pages.
        $page_match = !($visibility xor $page_match);
      }
      else {
        $page_match = drupal_eval($pages);
      }
    }
    else {
      $page_match = TRUE;
    }

  }
  return $page_match;
}

/**
 * Returns TRUE only if no other module objects to including Tynt.
 *
 * @see hook_tynt_service_visibility()
 */
function _tynt_service_visibility_custom() {
  static $custom_match;

  if (!isset($custom_match)) {
    $res = module_invoke_all('tynt_service_visibility');
    $custom_match = (array_search(FALSE, $res, TRUE) === FALSE);
  }

  return $custom_match;
}
